﻿namespace Beilox.Cryptography.Extensions
{
    public static class StringExtensions
    {
        public static string ToReverse(this string input)
        {
            char[] charArray = input.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
