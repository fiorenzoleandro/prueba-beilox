﻿using System.Text;

namespace Beilox.Cryptography
{
    public class ReplacementCryptography : ISecurityElement
    {
        Func<int, int> _encryptionAlgorithm;
        Func<int, int> _decryptionAlgorithm;

        public ReplacementCryptography(Func<int, int> encryptionAlgorithm, Func<int, int> decryptionAlgorithm)
        {
            _encryptionAlgorithm = encryptionAlgorithm;
            _decryptionAlgorithm = decryptionAlgorithm;
        }

        public string Decrypt(string text)
        {
            char[] charArray = text.ToCharArray();
            char[] output = new char[charArray.Length];
            byte[] asciiArray = Encoding.ASCII.GetBytes(charArray);
            for (int i = 0; i < asciiArray.Length; i++)
            {
                var ascii = Convert.ToInt32(asciiArray[i]);
                var newAscii = _decryptionAlgorithm(ascii);
                output[i] = (char)newAscii;
            }
            return new string(output);
        }

        public string Encrypt(string text)
        {
            char[] charArray = text.ToCharArray();
            char[] output = new char[charArray.Length];           
            byte[] asciiArray = Encoding.ASCII.GetBytes(charArray);
            for (int i = 0; i < asciiArray.Length; i++)
            {
                var ascii = Convert.ToInt32(asciiArray[i]);
                var newAscii = _encryptionAlgorithm(ascii);
                output[i] = (char)newAscii;
            }
            return new string(output);
        }
    }
}
