﻿using Beilox.Cryptography.Extensions;

namespace Beilox.Cryptography
{
    public class CustomCryptography : ISecurityElement
    {
        public int Position { get; set; }
        public int Factor { get; set; }

        public CustomCryptography(int position, int factor)
        {
            Position = position;
            Factor = factor;
        }

        public string Decrypt(string text)
        {
            char[] inputAsChar = text.ToCharArray();
            var firstCharacters = inputAsChar.Take(Position).ToArray();
            var lastCharaters = inputAsChar.TakeLast(inputAsChar.Length - Position).ToArray();

            var firstCharactersEncrypted = firstCharacters
                .Select(c => (char)(Convert.ToInt32(c) / Factor))
                .ToArray();

            return new string(firstCharactersEncrypted.Concat(lastCharaters).ToArray());
        }

        public string Encrypt(string text)
        {
            char[] inputAsChar = text.ToCharArray();
            var firstCharacters = inputAsChar.Take(Position).ToArray();
            var lastCharaters = inputAsChar.TakeLast(inputAsChar.Length - Position).ToArray();

            var firstCharactersEncrypted = firstCharacters
                .Select(c => (char)(Convert.ToInt32(c) * Factor))
                .ToArray();

            return new string(firstCharactersEncrypted.Concat(lastCharaters).ToArray());
        }
    }
}
