﻿namespace Beilox.Cryptography.CoR
{
    public abstract class AbstractHandler : IHandler
    {
        private IHandler _nextHandler;

        public object Handle(object request)
        {
            if(this._nextHandler is null)
                return null;
            return this._nextHandler.Handle(request);
        }

        public IHandler SetNext(IHandler nextHandler)
        {
            this._nextHandler = nextHandler;
            return nextHandler;
        }
    }
}
