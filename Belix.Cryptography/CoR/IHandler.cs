﻿namespace Beilox.Cryptography.CoR
{
    public interface IHandler
    {
        IHandler SetNext(IHandler handler);
        object Handle(object request);
    }
}
