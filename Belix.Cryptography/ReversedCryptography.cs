﻿using Beilox.Cryptography.Extensions;

namespace Beilox.Cryptography
{
    public class ReversedCryptography : ISecurityElement
    {
        public string Decrypt(string text)
        {
            return text.ToReverse();
        }

        public string Encrypt(string text)
        {
            return text.ToReverse();
        }
    }
}