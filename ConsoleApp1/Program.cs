﻿using Beilox.Cryptography;

namespace PruebaTecnica
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var textToProcess = "Hola Mundo";

            Console.WriteLine($"Texto original: {textToProcess}");

            List<ISecurityElement> securityElements = new List<ISecurityElement>()
            {
                new ReversedCryptography(),
                new ReplacementCryptography(EncryptionAlgorithm, DecryptionAlgorithm),
                new CustomCryptography(2, 2)
            };

            foreach (var securityElement in securityElements)
            {
                textToProcess = securityElement.Encrypt(textToProcess);
                Console.WriteLine(textToProcess);
            }

            Console.WriteLine($"Texto encriptado: {textToProcess}");

            securityElements.Reverse();

            foreach (var securityElement in securityElements)
            {
                textToProcess = securityElement.Decrypt(textToProcess);
                Console.WriteLine(textToProcess);
            }

            Console.WriteLine($"Texto desencriptado: {textToProcess}");

            Console.ReadKey();
        }

        static int EncryptionAlgorithm(int ascii)
        {
            return ascii + 1;
        }

        static int DecryptionAlgorithm(int ascii)
        {
            return ascii - 1;
        }
    } 

}